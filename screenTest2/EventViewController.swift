//
//  EventViewController.swift
//  screenTest2
//
//  Created by arifrahman on 3/16/17.
//  Copyright © 2017 arifrahman. All rights reserved.
//

import UIKit
import SVPullToRefresh

class EventViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    static func instantiateNav() -> UINavigationController {
        let nav = UIStoryboard.main.instantiateViewController(withIdentifier: "EventViewNav") as! UINavigationController
        //        let controller = nav.topViewController as! HomeViewController
        return nav
    }
    
    static func instantiateViewController() -> EventViewController {
        let controller = UIStoryboard.main.instantiateViewController(withIdentifier: "EventView") as! EventViewController
        return controller
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
        self.view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
        
        tableView.register(UINib(nibName: "ScreenEvent", bundle: nil), forCellReuseIdentifier: "ScreenEvent")
        
    }
    
    @IBAction func buttonMapViewPressed(_ sender: Any) {
        let controller = MapViewController.instantiateViewController()
        self.navigationController?.pushViewController(controller, animated: true)
    }

}

extension EventViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ScreenEvent", for: indexPath) as? ScreenEvent
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //navigationController.push(navController, animated:true)
    }
}
