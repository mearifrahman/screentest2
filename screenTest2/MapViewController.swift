//
//  MapViewController.swift
//  screenTest2
//
//  Created by arifrahman on 3/16/17.
//  Copyright © 2017 arifrahman. All rights reserved.
//

import UIKit
import ImageSlideshow
import GoogleMaps

class MapViewController: UIViewController {
    
    open override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        get {
            return .portrait
        }
    }
    
    static func instantiateNav() -> UINavigationController {
        let nav = UIStoryboard.main.instantiateViewController(withIdentifier: "MapViewNav") as! UINavigationController
        //        let controller = nav.topViewController as! HomeViewController
        return nav
    }
    
    static func instantiateViewController() -> MapViewController {
        let controller = UIStoryboard.main.instantiateViewController(withIdentifier: "MapView") as! MapViewController
        return controller
    }

    @IBOutlet weak var imageSlide: ImageSlideshow!
    @IBOutlet weak var mapView: GMSMapView!

    var circular = true
    var slideshowTimer: Timer?
    var slideshowTransitioningDelegate: ZoomAnimatedTransitioningDelegate?
    
    let camera = GMSCameraPosition.camera(withLatitude: -1.265386, longitude: 116.831200, zoom: 6.0)
    let camera2 = GMSCameraPosition.camera(withLatitude: -6.914744, longitude: 107.609810, zoom: 6.0)
    let camera3 = GMSCameraPosition.camera(withLatitude: -5.147665, longitude: 119.432732, zoom: 6.0)
    let camera4 = GMSCameraPosition.camera(withLatitude: -8.499112, longitude: 140.404984, zoom: 6.0)
    
    let localSource = [ImageSource(imageString: "img1")!, ImageSource(imageString: "img2")!, ImageSource(imageString: "img3")!, ImageSource(imageString: "img4")!]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageSlide.backgroundColor = UIColor.white
        imageSlide.slideshowInterval = 5.0
        imageSlide.pageControlPosition = PageControlPosition.underScrollView
        imageSlide.pageControl.currentPageIndicatorTintColor = UIColor.lightGray;
        imageSlide.pageControl.pageIndicatorTintColor = UIColor.black;
        imageSlide.contentScaleMode = UIViewContentMode.scaleAspectFill
//        imageSlide.scrollView.delegate = self
        imageSlide.currentPageChanged = { page in
            print("current page:", page)
            if self.imageSlide.currentPage == 0 {
                self.mapView.animate(to: self.camera)
            } else if self.imageSlide.currentPage == 1 {
                self.mapView.animate(to: self.camera2)
            } else if self.imageSlide.currentPage == 2 {
                self.mapView.self.animate(to: self.camera3)
            } else if self.imageSlide.currentPage == 3 {
                self.mapView.self.animate(to: self.camera4)
            }
        }
        imageSlide.setImageInputs(localSource)
    
        // marker
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: -1.265386, longitude: 116.831200)
        marker.title = "Balikpapan"
        marker.snippet = "Indonesia"
        marker.map = mapView
        
        let marker2 = GMSMarker()
        marker2.position = CLLocationCoordinate2D(latitude: -6.914744, longitude: 107.609810)
        marker2.title = "Bandung"
        marker2.snippet = "Indonesia"
        marker2.map = mapView
        
        let marker3 = GMSMarker()
        marker3.position = CLLocationCoordinate2D(latitude: -5.147665, longitude: 119.432732)
        marker3.title = "Makasar"
        marker3.snippet = "Indonesia"
        marker3.map = mapView
        
        let marker4 = GMSMarker()
        marker4.position = CLLocationCoordinate2D(latitude: -8.499112, longitude: 140.404984)
        marker4.title = "Merauke"
        marker4.snippet = "Indonesia"
        marker4.map = mapView
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(MapViewController.didTap))
        imageSlide.addGestureRecognizer(recognizer)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func didTap() {
        imageSlide.presentFullScreenController(from: self)
    }
    
}

//extension MapViewController: UIScrollViewDelegate {
//    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if circular {
//            let regularContentOffset = scrollView.frame.size.width * CGFloat(imageSlide.images.count)
//            
//            if (scrollView.contentOffset.x >= scrollView.frame.size.width * CGFloat(imageSlide.images.count + 1)) {
//                scrollView.contentOffset = CGPoint(x: scrollView.contentOffset.x - regularContentOffset, y: 0)
//            } else if (scrollView.contentOffset.x < 0) {
//                scrollView.contentOffset = CGPoint(x: scrollView.contentOffset.x + regularContentOffset, y: 0)
//            }
//            
//            
//
//        }
//    }
//    
//}
