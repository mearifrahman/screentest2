//
//  ScreenEvent.swift
//  screenTest2
//
//  Created by arifrahman on 3/16/17.
//  Copyright © 2017 arifrahman. All rights reserved.
//

import UIKit

class ScreenEvent: UITableViewCell {
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelTag: UILabel!
    @IBOutlet weak var labelContent: UILabel!
    @IBOutlet weak var imageAvatar: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
}
