//
//  ViewController.swift
//  screenTest2
//
//  Created by arifrahman on 3/16/17.
//  Copyright © 2017 arifrahman. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textFieldNama: UITextField!
    
    static func instantiateNav() -> UINavigationController {
        let nav = UIStoryboard.main.instantiateViewController(withIdentifier: "ViewControllerNav") as! UINavigationController
        //        let controller = nav.topViewController as! HomeViewController
        return nav
    }
    
    static func instantiateViewController() -> ViewController {
        let controller = UIStoryboard.main.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        return controller
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    
    @IBAction func buttonSelesaiPressed(_ sender: Any) {
        if let kalimat = textFieldNama.text {
            let palindrome = kalimat
            let reverse = String(palindrome.characters.reversed())
            
            if (reverse == palindrome) {
                let alert = UIAlertController(title: "Success", message: "\(kalimat) is a palindrome", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default) { (action) -> Void in
                    let controller = EventViewController.instantiateViewController()
                    self.navigationController?.pushViewController(controller, animated: true)
                }
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                //print("\(DestViewController.LabelText) is a palindrome")
            } else {
                let alert = UIAlertController(title: "Success", message: "\(kalimat) is not a palindrome", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default) { (action) -> Void in
                    let controller = EventViewController.instantiateViewController()
                    self.navigationController?.pushViewController(controller, animated: true)
                }
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }

}

extension UIStoryboard {
    static var main: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
}

